# Furby

These are handcrafted transcriptions of the firmware code used in the 1998 Furby.
It originates from the patent wrapper which was obtained by Sean Riddle and whose scans are available here:

- https://seanriddle.com/furbysource.pdf
- https://archive.org/details/furby-source

## Status

As all 296 pages will definitely not be transcribed by the current authors, only the following content is available yet:

| File Name     | Pages     | Finished | Issues | Description                                                           |
|---------------|:---------:|:--------:|:------:|-----------------------------------------------------------------------|
| `Furby35.asm` |   1 - 108 |       0% |      0 | Main program.                                                         |
| `Wake2.asm`   | 109 - 110 |     100% |      0 | Wake up method detection.                                             |
| `Light5.asm`  | 111 - 115 |     100% |      0 | Light sensor handling.                                                |
| `Diag7.asm`   | 116 - 125 |     100% |      1 | Diagnostics mode for factory resets / tests of sensors and hardware.  |
| `Furby27.inc` | 126 - 296 |       0% |      0 | Macro (speech and motor movement) lookup table data.                  |
| **Total**     |   1 - 296 |       5% |      1 |                                                                       |

## Formatting

The original source was stored in CP437 encoding, but printed in Windows 1252, causing ANSI borders to look wrong.
Additionally, the printer did not support several of the resulting characters and simply ignored them. To prevent such
issues, the transcriptions are all stored in UTF-8 encoding (without BOM).

Apparently, 8 characters were used for indentation with mixed tabs and spaces, as some commented out parts do not indent
the remainder of the line. The transcriptions use 8 spaces for indentation to allow character-perfect copies. White
space at the end of files is not preserved.

Text that is ineligible due to scan quality _issues_ is replaced with question marks if it cannot be restored logically
or unambiguously.
